Shader "Custom/NewSurfaceShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _RTSize("RTSize", Float) = 0.0
        _RTMaskTex("Mask Texture", 2D) = "white" {}
        _Position("Position", Vector) = (0,0,0,0)
        _Size("Size", Float) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="TransparentCutout" }
        LOD 200
        ZTest On

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard vertex:vert
        #pragma multi_compile_instancing
        #pragma instancing_options procedural:setup
        #pragma target 4.5


        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
        StructuredBuffer<float4x4> _MatrixBuffer;
#endif

        struct Input
        {
            float2 uv_MainTex;
            float3 pos;
            float3 dir;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        sampler2D _RTMaskTex;
        float4 _RTMaskTex_ST;
        float3 _RTPosition;
        half _RTSize;
        float3 _Position;
        float _Size;

        void setup()
        {
        #ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
            float4x4 data = _MatrixBuffer[unity_InstanceID];

            unity_ObjectToWorld = data;
        #endif
        }

        void vert(inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input, o); // Required for setting up an output

            float displaceMask = saturate(pow(v.texcoord.y, 2));
            float3 worldPos = mul(unity_ObjectToWorld, v.vertex);

            float3 dir = normalize(_Position - worldPos);
            float size = saturate(1.0 - pow(distance(_Position, worldPos) / _Size, 5));

            o.dir = dir * size * displaceMask;
            v.vertex.xyz += dir * size * displaceMask;

            o.pos = worldPos;
        }


        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float2 RTToWorld = IN.pos.xz - _RTPosition.xz;
            RTToWorld /= (_RTSize * 2.0);
            RTToWorld += 0.5;

            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            float3 mask = tex2Dlod(_RTMaskTex, float4(RTToWorld, 0, 0));
            float displaceMask = saturate(pow(IN.uv_MainTex.y + 0.05, 2));
            o.Albedo = c.rgb + (displaceMask * 0.5);
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            clip(c.a - lerp(0.9, 1000, step(0.001 ,mask.r)));
        }
        ENDCG
    }
    FallBack "Diffuse"
}
