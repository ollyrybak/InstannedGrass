using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RandomLight : MonoBehaviour
{
    [ContextMenu("Yo")]
    public void RandomColor()
    {
        GetComponent<Light>().color = Random.ColorHSV(0,1);
    }
}
