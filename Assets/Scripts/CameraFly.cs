using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFly : MonoBehaviour
{
    float pitch, yaw;

    // Start is called before the first frame update
    void Awake()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        var mouseDelta = new Vector2(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"));
        var inputVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        var shift = Input.GetKey(KeyCode.LeftShift);

        pitch += mouseDelta.x;
        yaw += mouseDelta.y;

        transform.Translate(inputVector * Time.deltaTime * (shift ? 3 : 2));
        transform.rotation = Quaternion.Euler(new Vector3(pitch, yaw, 0));

        if (Input.GetMouseButtonDown(0))
        {
            var ray = GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f, 0.5f));
            if (Physics.Raycast(ray, out var hit)) 
            {
                var go = GameObject.CreatePrimitive(PrimitiveType.Plane);
                go.transform.position = hit.point + new Vector3(0,0.01f, 0);
                go.layer = LayerMask.NameToLayer("Mask");
                    
            }
        }
    }

    private void OnGUI()
    {
        GUILayout.Label("Cick mouse to spawn plane that clips grass.");
        GUILayout.Label("Alt-F4 to quit.");
    }
}
